# bump-the-version 

**Made with <3 by [stdmatt](http://stdmatt.com).**

## Description:

```bump-the-version``` is a very small tool that helps me to manage the version strings in the source files.

I use [semantic versioning](https://semver.org) in my projects and with ```bump-the-version``` I don't need 
to "care" which specific version number I want to create, but the _semantic_ of the version that I want to create.

While it can be used as an stand-alone tool, most of the times I use that embedded into the build scripts.

<br>

As usual, you are **very welcomed** to **share** and **hack** it.

## Examples:

Let's assume that we have a ```#define PROGRAM_VERSION "1.0.0"``` at the ```program.c```.  
We made some changes and now we need to generate a new version of it... 

```shell script
$ bump-the-version "program.c" "#define PROGRAM_VERSION" bump baby  ## This will bump by 1 the x.x.BABY

## A possible ouput would be:
Old version: 2.0.1
New version: 2.0.2
```

Notice that when bump a version component with higher magnitude, the lesser 
components are reset to 0 automatically. 

```shell script
$ bump-the-version "program.c" "#define PROGRAM_VERSION" show  ## Show the current version.
2.0.2

$ bump-the-version "program.c" "#define PROGRAM_VERSION" bump minor ## This will reset "baby".
Old version: 2.0.2
New version: 2.1.0


$ bump-the-version "program.c" "#define PROGRAM_VERSION" bump major ## This will reset "minor" and "baby".
Old version: 2.1.0
New version: 3.0.0
```


## Usage:
```shell script
Usage:
  bump-the-version [--help] [--version]
  bump-the-version <file> <pattern> show
  bump-the-version <file> <pattern> bump <major  | minor | baby>

Options:
    *--help    : Show this screen.
    *--version : Show program version and copyright.

    <file>    : Path to the file that version will bumped.
    <pattern> : The string that can be used to uniquely identify the version line.

    info          : Shows the current version.
    bump <factor> : Bump the version by the factor. The only acceptable values
                    are major, minor or baby.
Notes:
  Options marked with * are exclusive, i.e. the bump-the-version will run that
  and exit after the operation.
```


## Install:

```shell script
## 1- Clone...
$ git clone https://gitlab.com/stdmatt-personal/bump_the_version.git
## 2 - Go to directory...
$ cd ./bump_the_version
## 3 - Install...
$ ./install.sh ## sudo password will be prompted to you
## 4 - Have fun ;D
```

## License:

This software is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).


## Others:

There's more FLOSS things at [stdmatt.com](https://stdmatt.com) :)
